// console.log('hello')

//advance task answer key:
let courseForm = document.querySelector('#createCourse');

courseForm.addEventListener("submit", (e) => {
	e.preventDefault(); //which is use to avoid automatic page redirection.

	let cName = document.querySelector('#courseName').value;
		// console.log(cName); //console.log() - is just to check if it works
		let pName =  document.querySelector('#coursePrice').value;
		// console.log(pName);
		let cDesc = document.querySelector('#courseDescription').value;
		// console.log(cDesc);


		if((cName !== "") && (pName !== "") && (cDesc !== "")) {
			fetch('https://murmuring-inlet-52021.herokuapp.com/api/courses/courseName-exists', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					name: cName
				})
			}).then(res => res.json())

		// console.log(res)
		.then(data => {
			if(data === false) {

				fetch('https://murmuring-inlet-52021.herokuapp.com/api/courses/addCourse', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						name: cName,
						price: pName,
						description: cDesc
					})
				}).then(res => {
						return res.json() //after describing the structure of the request body, now create the structure of the response coming from the backend.
					}).then(info => {
						if(info === true) {
							Swal.fire({
								icon: 'success',
								title: 'Success!', //the value of the title will be up to the dev.
								text: 'A new course is successfully created.'//can be used to display/show more details/information about the action/response.
							})
						} else {
							Swal.fire({
								icon: 'error',
								title: 'Whoooops!!', //the value of the title will be up to the dev.
								text: 'Something went wrong when adding a new course.'//can be used to display/show more details/information about the action/response.
							})
						}
					})
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Whoooops!!', //the value of the title will be up to the dev.
						text: 'Course Already Exists.'//can be used to display/show more details/information about the action/response.
					})
			}
		})
	} else {
		Swal.fire({
			icon: 'error',
			title: 'Whoooops!!', //the value of the title will be up to the dev.
			text: 'Something went wrong with your credentials.'//can be used to display/show more details/information about the action/response.
		})
	}
})
			// console.log(data)


			// let existingName = req.body.name.split("").map(letter => letter.tolowerCase());
			// cName.innerHTML = data.name.split("").map(letter => letter.toLowerCase());
			// existingName.length = cName.innerHTML;
			// 	// existingName.forEach(function(a) {
			// 	// })
			// 	existingName.forEach((a) => {
			// 	})
			// 	addedName.forEach((b) => {
			// 	})
			// existingName = existingName.join("")
			// addedName = addedName.join("")
			// if(addedName === existingName) {
			// 	console.log("Already Exists")
			// } else {
			// 	console.log("Succesfull")
			// }