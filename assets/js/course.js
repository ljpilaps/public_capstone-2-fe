console.log('Hello from the other side');

//the first thing that we need to do is to identify which is need to display inside the browser.
//we are going to use the course id to identify the correct course properly.
let params = new URLSearchParams(window.location.search)
//window - represents browser's window.
//window.location - returns a location object with information about the current location of the document.
//.search - contains the query string section of the current.
//URL. //search property returns an object of type stringString.
//URLSearchParams() - this method/constructor creates and returnss a new URLSearchParans object. (this is a class).
//"URLSearchParams" - describes the interface that defines utility methods to work with query string of a URL. (this is the prop type).
//new -> insatantiate a user-defiend object.
//new - - also creates new object

/* params = {
	"courseId": "...id ng course that we passed"
   }
*/
let id = params.get('courseId')
console.log(id)

//lets capture the access token from the local storage.
let token = localStorage.getItem('token')
console.log(token); //this is added only for educational purposes.

//lets capture the sections of the html body

let cName = document.querySelector("#courseName")
let cDesc = document.querySelector("#courseDesc")
let cPrice = document.querySelector("#coursePrice")
let enroll = document.querySelector("#enrollmentContainer")


/* stress goal */
// let viewCourseUrl = `http://localhost:4000/api/courses/${id}`;
// let enrollCourseUrl = `http://localhost:4000/api/users/enroll/`;

/* stress goal*/
// 1st fetch - `https://murmuring-inlet-52021.herokuapp.com/api/courses/${id}`

fetch(`http://localhost:4000/api/courses/${id}`).then(res => res.json()).then(data => {
	console.log(data) //if the result is displayed in the console, it means you were able to properly pass the course id and successfully created your fetch request.
	cName.innerHTML = data.name
	cDesc.innerHTML = data.description
	cPrice.innerHTML = data.price
	enroll.innerHTML = `<a id="enrollButton">Enroll</a>`
	
/* stress goal */

//FIRST: capture the element/anchor tag for enroll
//SECOND: add an event listener to trigger an event
	document.querySelector('#enrollButton').addEventListener("click", () => {
//THIRD: insert the course to the enrolmments array inside the user collection/profile collection(in frontend)
		fetch('http://localhost:4000/api/users/enroll', {
//FOURTH: describe the parts of the request.
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
//FIFTH: get the value of auth string to verify and validate the user. (to figure out if it's admin or regular user. If admin it CAN'T enroll, but if it's regular user it CAN enroll)
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
//SIXTH: to get the id of the course see line 33
				courseId: id //check line 33
			})
//SEVENTH: create a promise to get the response			
		}).then(res => { //use local first for testing
			return res.json()
		}).then(data => {
//EIGHT: inform the user that the request has been done.
			if(data === true) {
				alert("Thank you for enrolling to this course.")
//NINTH: redirect back to courses page.
				window.location.replace("./courses.html")
			} else {
//TENTH: if failed to enroll/request has failed.
				alert("something went wrong.")
			}
		})

	})
})