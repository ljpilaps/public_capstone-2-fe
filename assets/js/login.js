console.log("hello World");

let loginForm = document.querySelector('#loginUser')

loginForm.addEventListener("submit", (e) => {
	e.preventDefault();
let email = document.querySelector("#userLoginEmail").value
console.log(email);
let password = document.querySelector("#password").value
console.log(password);



//NOW: how can we inform a user that a blank input field cannot be logged in?
	if(email === "" || password === "") {
		Swal.fire({
			icon: 'error',
			title: 'Whoooops!!', //the value of the title will be up to the dev.
			text: "Please input your email and/or password."//can be used to display/show more details/information about the action/response.
		})
	} else {
		fetch('https://murmuring-inlet-52021.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(res => {
			return res.json();
		}).then(data => {
			console.log(data.access)
			if(data.access) {
				//lets save the access token inside our local storage
				localStorage.setItem('token', data.access) //for saving to the localStorage
				//this local storage can be found inside the subfolder of the appData of the local file of the google chrome browser inside the data module of the user folder.
				// \AppData\Local\Google\Chrome\User Data\Default\Local Storage
				Swal.fire({
					icon: 'success',
					title: 'Success!', //the value of the title will be up to the dev.
					text: 'Access key saved on local storage' //can be used to display/show more details/information about the action/response.
				}) //for educational purposes to see if succesfully saved on local storage.
				fetch(`https://murmuring-inlet-52021.herokuapp.com/api/users/details`, {
					headers: {
						'Authorization': `Bearer ${data.access}`
					}
				}).then(res => {
					return res.json()
				}).then(data => {
					console.log(data)
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
					console.log("items are set inside the local storage.")
					//we are trying to direct the usr to the courses page upon successful login.SS
					// window.location.replace('./index.html')
					window.location.replace('./pages/courses.html')
				})
			} else {
				//if there is no existing access key value from the data variable then just inform the user.
					Swal.fire({
						icon: 'error',
						title: 'Whoooops!!', //the value of the title will be up to the dev.
						text: 'Something went wrong with your credentials.'//can be used to display/show more details/information about the action/response.
					})
			}
		})
	}

})