console.log('awit');

let registerForm = document.querySelector('#registerUser');

registerForm.addEventListener("submit", (e) => {
	e.preventDefault(); //to avoid page refresh/ redirection once the event has been triggered.

//capture each value inside input fields inside html file(register)
let fName = document.querySelector('#firstName').value
console.log(fName);

let lName = document.querySelector('#lastName').value
console.log(lName);

let userEmail = document.querySelector('#userRegisterEmail').value
console.log(userEmail);

let mNum = document.querySelector('#mobileNumber').value
console.log(mNum);

let password = document.querySelector('#password1').value
console.log(password);

let verifyPassword = document.querySelector('#password2').value
console.log(verifyPassword);

//information validation upon creating a new entry in the database.
//lets create a control structure
//to check if passwords match
//to check if password are not empty
//to check the validation for mobile number, what wecan do is to check the length of the mobile number input
if((password !== "" && verifyPassword !== "") && (verifyPassword === password) && (mNum.length === 11)) {
	fetch('https://murmuring-inlet-52021.herokuapp.com/api/users/email-exists', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			email: userEmail
		})
	}).then(res => res.json()) //this will give the information if there are no duplicates found
	// console.log(res)
	.then(data => {
		console.log(data)
		if(data === false) { //if nag false sa controllers(user.js - emailExists) ito ang mangyayari
			fetch('https://murmuring-inlet-52021.herokuapp.com/api/users/register', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstName: fName,
					lastName: lName,
					email: userEmail,
					mobileNo: mNum,
					password: password,
			}) //this section describes the body of the request converted into a JSON format.
			}).then(res => {
				// console.log(res.body.fName)
				return res.json();
			}).then(data => {
				if(data === true) {
					Swal.fire("New Account Registered Successfully"); //if nag true don sa controllers(user.js - register) ito ang mangyayari
				} else {
					Swal.fire("Failed to Register"); //if false naman ito.
				}
			})
		} else {
			Swal.fire("email already exists, choose another email") //if nag true don sa controllers(user.js -emailExists) ito yung mangyayari
		}
	})
} else {
	Swal.fire({
		icon: 'error',
		title: 'Whoooops!!', //the value of the title will be up to the dev.
		text: 'Finish the form first before clicking register.'//can be used to display/show more details/information about the action/response.
	}) //for password and number length
}
})