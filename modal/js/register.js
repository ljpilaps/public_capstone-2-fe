/* register modal*/
let modalRegBtn = document.querySelector('.regButton');
let modalRegBg = document.querySelector('.overlay-register-bg');
let modalRegClose = document.querySelector('.reg-close-btn');

modalRegBtn.addEventListener('click', () => {
	modalRegBg.classList.add('register-bg-active');
})

modalRegClose.addEventListener('click', () => {
	modalRegBg.classList.remove('register-bg-active');
})